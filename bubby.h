static const char norm_fg[] = "#ccccc6";
static const char norm_bg[] = "#0b0a0f";
static const char norm_border[] = "#9f9f9f";

static const char sel_fg[] = "#ccccc6";
static const char sel_bg[] = "#8e5e9c";
static const char sel_border[] = "#ffffff";

static const char *colors[][3]      = {
    /*               fg           bg         border                         */
    [SchemeNorm] = { norm_fg,     norm_bg,   norm_border }, // unfocused wins
    [SchemeSel]  = { sel_fg,      sel_bg,    sel_border },  // the focused win
};
